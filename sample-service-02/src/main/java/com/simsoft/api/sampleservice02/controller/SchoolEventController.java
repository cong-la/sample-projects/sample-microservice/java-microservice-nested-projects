package com.simsoft.api.sampleservice02.controller;

import com.simsoft.api.sampleservice01.model.SchoolInfo;
import com.simsoft.api.sampleservice02.model.SchoolEvent;
import com.simsoft.api.sampleservice02.repository.SchoolEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/event")
public class SchoolEventController {

    @Autowired
    private SchoolEventRepository schoolEventRepository;

    @GetMapping()
    public ArrayList<SchoolEvent> findAll() {
        return this.schoolEventRepository.findAll();
    }

    @GetMapping(path = "/school/{schoolId}")
    public ArrayList<SchoolEvent> findBySchoolId(@PathVariable("schoolId") int schoolId) {
        return this.schoolEventRepository.findBySchoolId(schoolId);
    }

    @GetMapping(path = "/{id}")
    public SchoolEvent findById(@PathVariable("id") int id) {
        return this.schoolEventRepository.findById(id);
    }

    @PostMapping(path = "/event/add")
    public SchoolEvent save(@RequestBody SchoolEvent schoolEvent) {
        return this.schoolEventRepository.save(schoolEvent);
    }
}
