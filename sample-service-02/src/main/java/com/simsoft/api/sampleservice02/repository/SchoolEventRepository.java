package com.simsoft.api.sampleservice02.repository;

import com.simsoft.api.sampleservice02.model.SchoolEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface SchoolEventRepository extends JpaRepository<SchoolEvent, Integer> {

    public ArrayList<SchoolEvent> findAll();

    public ArrayList<SchoolEvent> findBySchoolId(int schoolId);

    public SchoolEvent findById(int id);

    public SchoolEvent save(SchoolEvent schoolEvent);
}
